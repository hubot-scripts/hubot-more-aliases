# Hubot-more-aliases
Do you like nicknames? I bet our little friend, Hubot, likes them, too. 

Unfortunately, you can only name him twice: once
for his real name (`robot.name`) and once for his alias (`robot.alias`). What if you want to have multiple aliases? 
Well, this is where the `hubot-more-aliases` comes to play.

## Usage

### Installing the module

First step is to include the `hubot-more-aliases` dependency to your Hubot scripts (inside the package.json file). Then
all you have to do is to update your `external-scripts.json` file to include our library. If this file is missing, simply
create it with content:
```json
["hubot-more-aliases"]
```

### Set up the aliases

The library will load Hubot's aliases from the `HUBOT_ALIASES` environment variable. They should be separated with coma
and can contain spaces. Please keep in mind, that you can also use RegExp inside alias. Some examples:
```text
Fancy\s+bot
Hubot[s]?\s+(?:name|alias)
```
As you can see, it is good idea to use `\s+` instead of simple ` `, because the user can sometimes hit the space twice.
Also please use the `(?:)` notation, because otherwise it will mess up normal parameters order.

### Try using the alias!

After starting the Hubot, you will be able to use new alias. Similiar to normal HUBOT_ALIAS, you can also use `:` and `,`
after the bot's name. Inside the logs you will find information about proxying the message to different name.

## How does it work?

The library has created new Hubot's script that is listening inside the room for every alias. After the bot will find
the message directed to alias, it will remove the message from processing queue and then create new message with the bot's
name changed to the original one (`robot.name`).

## Contribution
If you want to contribute, just fork the project and add some changes!

## Errors and ideas to improve
If you will find any errors with this library or have great idea how to improve it (and don't want to do it on your own),
please feel free to open [a new ticket](https://gitlab.com/hubot-scripts/hubot-more-aliases/issues). 
