'use strict';

// Description:
//   Provides a way to define more aliases for the Hubot
//
// Dependencies:
//   None
//
// Configuration:
//   HUBOT_ALIASES - The list of aliases separated by coma. You can use regexp functionality when defining the bot's alias.
//
// Commands:
//   None
//
// Author:
//   Dorian Krefft <dorian.krefft@gmail.com>


const TextMessage = require("hubot").TextMessage;

module.exports = (robot) => {
    const aliases = getAliases(process.env.HUBOT_ALIASES);

    if (aliases) {
        robot.logger.info(`Loaded ${aliases.length} aliases: ${aliases.join(', ')}. Robot name: ${robot.name}, alias: ${robot.alias}`);

        const regexp = generateRegExp(aliases);

        robot.hear(regexp, (response) => {
            response.finish();
            const usedAlias = response.match[1];
            const message = response.match[2];
            const text = `${robot.name} ${message}`;

            robot.logger.info(`Received message to alias '${usedAlias}'. Sending message to the bot: '${text}'`);

            robot.receive(new TextMessage(response.message.user, text, response.message.id));
        });
    }
    else {
        robot.logger.warning(`You are using the hubot-more-aliases library, yet you did not define any aliases by setting the HUBOT_ALIASES environment variable!`);
    }

    function generateRegExp(aliases) {
        const aliasPart = aliases.join('|');
        return new RegExp(`^\\s*[@]?(${aliasPart})[:,]?\\s+(.+)`, 'i');
    }

    function getAliases(envVariable){
        return envVariable ? filterProblematicAliases(envVariable.split(',')) : null;
    }

    function filterProblematicAliases(aliases){
        const filtered = aliases.filter((el) => {
            return ![robot.name, robot.alias].includes(el);
        });

        if (filtered.length < aliases.length) {
            robot.logger.warning(`At least one of the aliases contains robot name (${robot.name}) or alias (${robot.alias})!
                To avoid the problem with infinite loop, it was removed from the aliases list. Please consider removing those aliases.
                Original aliases list: ${aliases.join(', ')}`.replace(/^\s+/mg, ''));
        }
        return filtered;
    }
};