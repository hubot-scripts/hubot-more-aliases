'use strict';
const HubotChatTesting = require('hubot-chat-testing');
const Helper = require('hubot-test-helper');

describe('Using different names for the bot', () => {
    const chat = new HubotChatTesting('hubot', new Helper(['./helper-scripts/hello-world.js', '../scripts/index.js']));

    context('The bot does have some aliases defined', () => {
        beforeEach(() => {
            process.env.HUBOT_ALIASES = 'Another Name,Fancy Bot,Dorian,El Doriano Papino,Regexped\\s+(?:Name|Alias)';
        });

        chat.when('the user is calling the bot with is real name')
            .user('alice').messagesBot('hello')
            .bot.repliesWith('You are my world!')
            .expect('the bot should be able to respond properly');

        chat.when('the user is calling the bot with one of its aliases')
            .user('alice').messagesRoom('Dorian hello')
            .bot.repliesWith('You are my world!')
            .expect('the bot should be able to respond properly');

        chat.when('the user is calling the bot with its alias with multiple words')
            .user('alice').messagesRoom('el doriano papino hello')
            .bot.repliesWith('You are my world!')
            .expect('the bot should be able to respond properly');

        chat.when('the user is calling the bot with its alias with message with parameters')
            .user('alice').messagesRoom('fancy bot i\'m alice van guard')
            .bot.repliesWith('Hello alice van guard')
            .expect('the parameters should be passed properly');

        chat.when('the user is calling the bot with coma')
            .user('alice').messagesRoom('fancy bot, hello')
            .bot.repliesWith('You are my world!')
            .expect('the bot should be able to respond properly');

        chat.when('the user is calling the bot with colon')
            .user('alice').messagesRoom('fancy bot: hello')
            .bot.repliesWith('You are my world!')
            .expect('the bot should be able to respond properly');

        chat.when('the user is calling the bot with alias that is regexp')
            .user('alice').messagesRoom('Regexped    name hello')
            .bot.repliesWith('You are my world!')
            .user('alice').messagesRoom('regexped  alias hello')
            .bot.repliesWith('You are my world!')
            .expect('the bot should be able to respond properly');
    });

    context('The bot does not have any aliases defined', () => {
        beforeEach(() => {
            process.env.HUBOT_ALIASES = '';
        });

        chat.when('the user is calling the bot with is real name')
            .user('alice').messagesBot('hello')
            .bot.repliesWith('You are my world!')
            .expect('the bot should be able to respond properly');
    });

    context('The bot has duplicated defined', () => {
        beforeEach(() => {
            process.env.HUBOT_ALIASES = 'hubot';
        });

        chat.when('the user is calling the bot with is real name which is also a duplicate')
            .user('alice').messagesBot('hello')
            .bot.repliesWith('You are my world!')
            .expect('the bot should be able to respond properly whithout any problems or loops');
    });
});