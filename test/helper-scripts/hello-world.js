'use strict';

// This script is used only to test the main functionality. Please do not load it in any way.
module.exports = (robot) => {
    robot.respond(/hello/i, (response) => {
        response.finish();
        response.reply('You are my world!');
    });

    robot.respond(/i'm (.*)+/i, (response) => {
        response.finish();
        const name = response.match[1];
        response.reply(`Hello ${name}`);
    });
};